class AddComment < ActiveRecord::Migration[5.2]
  def change
    add_reference :Posts, :Comment, foreign_key: true
  end
end
