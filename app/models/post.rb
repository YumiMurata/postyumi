class Post < ApplicationRecord
    has_many :comments
    has_many :likes
    validates :usuario, presence true
end
