Rails.application.routes.draw do
  get '/posts/:id/likes', to: 'likes#posts'
  get '/posts/:id/comments', to: 'comments#posts'

  resources :likes
  resources :comments
  resources :posts


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
